import { Firestore } from '@google-cloud/firestore';
import { spawn, ChildProcessWithoutNullStreams } from 'child_process';
import { createInterface } from 'readline';
import {
  FirestoreRepository,
  FirestoreTransactionManager,
} from '../firestoreRepository';
import * as t from 'io-ts';
import { repoTests } from './repoTests';
import { DateFromTimestamp } from '../dateFromTimestamp';

describe('FirestoreRepository', () => {
  if (process.env.FIRESTORE_EMULATOR_HOST === undefined) {
    const testPort = 8999;
    process.env['FIRESTORE_EMULATOR_HOST'] = `localhost:${testPort}`;
    beforeAll(async () => {
      console.log('Spawning Firebase emulator.');
      emulator = spawn(
        'gcloud',
        [
          'beta',
          'emulators',
          'firestore',
          'start',
          '--host-port',
          `localhost:${testPort}`,
        ],
        { detached: true },
      );
      const rl = createInterface({
        input: emulator.stderr,
      });

      console.log('Waiting for emulator to start.');

      await new Promise((resolve, reject) => {
        let started = false;
        rl.on('line', line => {
          console.log('EMULATOR: ', line);
          if (!started && line.match(/Dev App Server is now running/)) {
            console.log('Emulator has started!');
            started = true;
            resolve();
          }
        });
      });
    }, 5000);

    afterAll(async () => {
      if (emulator?.pid) {
        console.log('Killing emulator.');
        process.kill(-emulator.pid);
      }
    });
  }

  const firestore = new Firestore();
  const repo = new FirestoreRepository(
    () => firestore.collection('people'),
    t.intersection([
      t.intersection([
        t.type({
          name: t.string,
          age: t.number,
        }),
        t.partial({
          birthDate: DateFromTimestamp,
          married: t.boolean,
        }),
      ]),
      t.union([
        t.type({
          type: t.literal('loner'),
        }),
        t.intersection([
          t.type({
            type: t.literal('pet lover'),
          }),
          t.partial({
            pets: t.array(t.string),
          }),
        ]),
        t.intersection([
          t.type({
            type: t.literal('pet hater'),
          }),
          t.partial({
            children: t.array(t.string),
          }),
        ]),
      ]),
    ]),
  );

  const tranManager = new FirestoreTransactionManager(firestore);
  let emulator: ChildProcessWithoutNullStreams;

  describe('repo tests', () => repoTests({ repo, tranManager }));
});
