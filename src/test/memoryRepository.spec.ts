import {
  MemoryRepository,
  MemoryTransactionManager,
} from '../memoryRepository';
import { repoTests, Person } from './repoTests';

describe('Repository', () => {
  const repo = new MemoryRepository<Person>();
  const tranManager = new MemoryTransactionManager();

  describe('repo tests', () => repoTests({ repo, tranManager }));
});
