import {
  Repository,
  ID,
  TransactionManager,
  Snapshot,
  DELETE,
} from '../repository';
import { takeWhile, toArray } from 'rxjs/operators';

export type Person = {
  name: string;
  age: number;
  birthDate?: Date;
  married?: boolean;
} & (
  | {
      type: 'pet lover';
      pets?: string[];
    }
  | {
      type: 'pet hater';
      children?: string[];
    }
  | {
      type: 'loner';
    }
);

export const repoTests = ({
  repo,
  tranManager,
}: {
  repo: Repository<Person>;
  tranManager: TransactionManager;
}) => {
  const mendoza = {
    name: 'Mendoza',
    age: 21,
    birthDate: new Date(1970, 1, 1),
    married: false,
    type: 'pet lover' as const,
  };
  const sambuca = {
    name: 'Sambuca',
    age: 31,
    birthDate: new Date(1960, 1, 1),
    married: false,
    type: 'pet lover' as const,
  };
  const fifi = {
    name: 'Fifi',
    age: 11,
    birthDate: new Date(1980, 1, 1),
    married: false,
    type: 'pet lover' as const,
  };
  const enrica = {
    name: 'Enrica',
    age: 21,
    birthDate: new Date(1970, 1, 1),
    married: false,
    type: 'pet lover' as const,
  };
  const latisha = {
    name: 'Latisha',
    age: 21,
    birthDate: new Date(1972, 1, 1),
    married: false,
    type: 'loner' as const,
  };
  const jemima = {
    name: 'Jemima',
    age: 21,
    birthDate: new Date(1972, 1, 1),
    married: true,
    type: 'pet hater' as const,
  };

  it('in out', async () => {
    const { id, createdAt } = await repo.insert(mendoza);
    const retrieved = await repo.getOne(id);
    expect(retrieved?.data).toMatchObject(mendoza);
    await repo.delete(id, { lastUpdated: createdAt });
    const retrieved2 = await repo.getOne(id);
    expect(retrieved2).toBeUndefined();
  });

  it('query', async () => {
    await repo.insert(mendoza, '3');
    await repo.insert(sambuca, '2');
    await repo.insert(fifi, '1');
    const results = await repo.get();
    expect(results.items.map(item => item.data)).toMatchObject([
      fifi,
      sambuca,
      mendoza,
    ]);
    expect(
      results.changes().map(change => ({ ...change, item: change.item.data })),
    ).toMatchObject([
      { item: fifi, type: 'added', oldIndex: -1, newIndex: 0 },
      { item: sambuca, type: 'added', oldIndex: -1, newIndex: 1 },
      { item: mendoza, type: 'added', oldIndex: -1, newIndex: 2 },
    ]);
    await repo.delete('1');
    await repo.delete('2');
    await repo.delete('3');
  });

  it('complex query', async () => {
    const ids = await Promise.all([
      repo.insert(mendoza, '3'),
      repo.insert(sambuca, '2'),
      repo.insert(fifi, '1'),
      repo.insert(enrica, '5'),
      repo.insert(latisha, '4'),
      repo.insert(jemima),
    ]);
    const results = await repo
      .where('married', '==', false)
      .where('age', '<', 31)
      .orderBy('age', 'desc')
      .thenBy('name', 'asc')
      .thenBy(ID, 'asc')
      .get();
    expect(results.items.map(item => item.data)).toMatchObject([
      enrica,
      latisha,
      mendoza,
      fifi,
    ]);
    const results2 = await repo
      .where('married', '==', false)
      .where('age', '<', 31)
      .orderBy('age', 'desc')
      .thenBy('name', 'desc')
      .thenBy(ID, 'asc')
      .startAfter(latisha.age, latisha.name, '3')
      .get();
    expect(results2.items.map(item => item.data)).toMatchObject([
      latisha,
      enrica,
      fifi,
    ]);
    await Promise.all(ids.map(id => repo.delete(id.id)));
  });

  it('snapshots', async () => {
    const listener = repo
      .onSnapshot()
      .pipe(
        takeWhile(snapshot => snapshot.items.length < 5, true),
        toArray(),
      )
      .toPromise();

    const ids = await Promise.all([
      repo.insert(mendoza, '5'),
      repo.insert(sambuca, '4'),
      repo.insert(fifi, '3'),
      repo.insert(enrica, '2'),
      repo.insert(latisha, '1'),
    ]);

    const snapshots = await listener;
    expect(
      snapshots[snapshots.length - 1].items.map(item => item.data),
    ).toMatchObject([latisha, enrica, fifi, sambuca, mendoza]);

    const items2 = snapshots.reduce<Snapshot<Person>[]>((prev, next) => {
      const old: (Snapshot<Person> | undefined)[] = [...prev];
      const working: (Snapshot<Person> | undefined)[] = new Array(
        next.items.length,
      ).fill(undefined);
      next.changes().forEach(change => {
        if (change.type === 'added' || change.type === 'modified') {
          working[change.newIndex] = change.item;
          old[change.oldIndex] = undefined;
        } else {
          old[change.oldIndex] = change.item;
        }
      });
      for (const remaining of old) {
        if (remaining !== undefined) {
          working[working.findIndex(item => item === undefined)] = remaining;
        }
      }
      return working as Snapshot<Person>[];
    }, []);
    expect(items2.map(item => item.data)).toMatchObject([
      latisha,
      enrica,
      fifi,
      sambuca,
      mendoza,
    ]);
    await Promise.all(ids.map(id => repo.delete(id.id)));
  });

  it('update', async () => {
    const { id } = await repo.insert(mendoza);
    await repo.update(id, {
      age: { incBy: 1 },
      married: true,
    });
    const retrieved = await repo.getOne(id);
    expect(retrieved?.data).toMatchObject({
      ...mendoza,
      age: 22,
      married: true,
    });
    await expect(
      repo.update(
        id,
        {
          age: { incBy: 2 },
          married: false,
        },
        {
          lastUpdated: BigInt(retrieved?.updatedAt) + BigInt(1000),
        },
      ),
    ).rejects.toBeDefined();
    await repo.update(
      id,
      {
        age: { incBy: 2 },
        married: false,
      },
      {
        lastUpdated: retrieved!.updatedAt,
      },
    );
    const retrieved2 = await repo.getOne(id);
    expect(retrieved2?.data).toMatchObject({
      ...mendoza,
      age: 24,
      married: false,
    });
    await repo.delete(id, {
      lastUpdated: retrieved2!.updatedAt,
    });
  });

  it('transactions', async () => {
    // ok - insert x 3, then tran to read all, insert, update and delete
    const ids = await Promise.all([
      repo.insert(mendoza),
      repo.insert(sambuca),
      repo.insert(fifi),
    ]);

    const ok = await tranManager.runTransaction(async tran => {
      const scope = tran.with(repo);

      const results = await scope.get(repo.where('name', '==', 'Fifi'));
      expect(results.items.map(item => item.data)).toMatchObject([fifi]);

      scope
        .insert(latisha)
        .update(
          ids[0].id,
          { name: 'Mundoza' },
          { lastUpdated: ids[0].createdAt },
        )
        .delete(ids[1].id, { lastUpdated: ids[1].createdAt });
      return true;
    });

    expect(ok).toBeTruthy();

    const all = await repo.orderBy('name').get();

    expect(all.items.map(item => item.data)).toMatchObject([
      fifi,
      latisha,
      { ...mendoza, name: 'Mundoza' },
    ]);

    await expect(
      tranManager.runTransaction(async tran => {
        const scope = tran.with(repo);
        const results = await scope.get(repo.where('name', '==', 'Fifi'));
        expect(results.items.map(item => item.data)).toMatchObject([fifi]);
        scope.delete(ids[0].id);
        scope.update(
          ids[2].id,
          { married: true },
          { lastUpdated: ids[0].createdAt }, // wrong time
        );
      }),
    ).rejects.toBeDefined();

    const all2 = await repo.orderBy('name').get();

    expect(all2.items.map(item => item.data)).toMatchObject([
      fifi,
      latisha,
      { ...mendoza, name: 'Mundoza' },
    ]);

    await Promise.all(all2.items.map(item => repo.delete(item.id)));
  });

  it('sentinels', async () => {
    const { id } = await repo.insert({
      ...mendoza,
      age: 101,
      birthDate: 'serverTimestamp',
      pets: ['dog', 'cat'],
    });
    const time = Date.now();
    const mendoza2 = await repo.getOne(id);
    expect(mendoza2?.data.birthDate?.getTime()).toBeCloseTo(time, -1000);
    await repo.update(id, {
      age: { incBy: 10 },
      pets: { add: ['bird', 'ferret'] },
    });
    await repo.update(id, {
      age: { incBy: -1 },
      pets: { remove: ['dog', 'cat'] },
    });
    await expect(repo.getOne(id)).resolves.toMatchObject({
      data: {
        ...mendoza2?.data,
        age: 110,
        pets: ['bird', 'ferret'],
      },
    });
    await repo.update(id, {
      pets: DELETE,
    });
    const { pets, ...rest } = { ...mendoza2?.data };
    const mendoza3 = await repo.getOne(id);
    expect(mendoza3?.data).toMatchObject({
      ...rest,
      age: 110,
    });
    expect(
      mendoza3?.data.type === 'pet lover' && mendoza3.data.pets,
    ).toBeUndefined();
    await repo.delete(id);
  });
};
