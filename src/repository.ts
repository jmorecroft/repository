import { Observable } from 'rxjs';

export interface Snapshot<T> {
  readonly id: string;
  readonly data: T;
  readonly createdAt: bigint;
  readonly updatedAt: bigint;
}

interface QuerySnapshotChange<T> {
  item: Snapshot<T>;
  type: 'added' | 'removed' | 'modified';
  oldIndex: number;
  newIndex: number;
}

export interface QuerySnapshot<T> {
  items: Snapshot<T>[];
  changes(): QuerySnapshotChange<T>[];
}

export const ID = Symbol('ID');
export const DELETE = Symbol('DELETE');

type NumberSentinel = { incBy: number };
type DateSentinel = 'serverTimestamp';
type ArraySentinel<T> = { add: T } | { remove: T };
type Deletable<T> = T extends undefined ? typeof DELETE : never;

export type WithSentinels<T> = {
  [K in keyof T]:
    | (NonNullable<T[K]> extends number
        ? T[K] | NumberSentinel
        : NonNullable<T[K]> extends Date
        ? T[K] | DateSentinel
        : NonNullable<T[K]> extends any[]
        ? T[K] | ArraySentinel<NonNullable<T[K]>>
        : T[K])
    | Deletable<T[K]>;
};

export type Operator<T> = NonNullable<T> extends any[]
  ? 'array-contains' | 'array-contains-any'
  : '<' | '<=' | '>=' | '>' | '==' | 'in';

export type OrderByDirection = 'asc' | 'desc';

type TupleAppend<TP, T> = TP extends []
  ? [T]
  : TP extends [any]
  ? [TP[0], T]
  : TP extends [any, any]
  ? [TP[0], TP[1], T]
  : TP extends [any, any, any]
  ? [TP[0], TP[1], TP[2], T]
  : never;

export type QueryBy<
  T,
  K extends keyof T | typeof ID,
  O extends any[] = []
> = Query<T, TupleAppend<O, K extends keyof T ? NonNullable<T[K]> : string>>;

export interface Query<T, C extends any[] = [string]> {
  where<K extends keyof T>(
    field: K,
    op: Operator<T[K]>,
    value: T[K],
  ): Query<T, C>;

  limit(limit: number): Query<T, C>;

  orderBy<K extends keyof T | typeof ID>(
    field: K,
    direction?: OrderByDirection,
  ): QueryBy<T, K>;

  thenBy<K extends keyof T | typeof ID>(
    field: K,
    direction?: OrderByDirection,
  ): QueryBy<T, K, C>;

  startAfter(...cursor: C): Query<T, C>;

  get(): Promise<QuerySnapshot<T>>;
  onSnapshot(): Observable<QuerySnapshot<T>>;
}

export interface Repository<T> extends Query<T> {
  getOne(id: string): Promise<Snapshot<T> | undefined>;
  insert(
    data: WithSentinels<T>,
    id?: string,
  ): Promise<{ id: string; createdAt: bigint }>;
  upsert(id: string, data: WithSentinels<T>): Promise<bigint>;
  update(
    id: string,
    data: Partial<WithSentinels<T>>,
    precondition?: { lastUpdated: bigint },
  ): Promise<bigint>;
  delete(id: string, precondition?: { lastUpdated: bigint }): Promise<bigint>;
}

export interface TranScope<T> {
  get(query: Query<T, any>): Promise<QuerySnapshot<T>>;
  getOne(id: string): Promise<Snapshot<T> | undefined>;
  insert(data: WithSentinels<T>, id?: string): TranScope<T>;
  upsert(id: string, data: WithSentinels<T>): TranScope<T>;
  update(
    id: string,
    data: Partial<WithSentinels<T>>,
    precondition?: { lastUpdated: bigint },
  ): TranScope<T>;
  delete(id: string, precondition?: { lastUpdated: bigint }): TranScope<T>;
}

export interface Transaction {
  with<T>(repo: Repository<T>): TranScope<T>;
}

export interface TransactionManager {
  runTransaction<U>(
    callback: (tran: Transaction) => Promise<U>,
    options?: { maxAttempts?: number },
  ): Promise<U>;
}
