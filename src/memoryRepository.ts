import {
  Query,
  Repository,
  ID,
  Operator,
  OrderByDirection,
  QueryBy,
  QuerySnapshot,
  Snapshot,
  WithSentinels,
  TranScope,
  Transaction,
  TransactionManager,
  DELETE,
} from './repository';
import { Observable, Subscriber } from 'rxjs';
import { v4 } from 'uuid';
import { sort, zip } from 'fp-ts/lib/Array';
import { getMonoid, Ord, getDualOrd, ord } from 'fp-ts/lib/Ord';
import { fold } from 'fp-ts/lib/Monoid';
import diff = require('diff-arrays-of-objects');
import { Either, left, right, isLeft } from 'fp-ts/lib/Either';

type QueryFn<T> = (store: Map<string, Snapshot<T>>) => Snapshot<T>[];

interface MemoryQueryOptions<T> {
  orderBy: [keyof T | typeof ID, OrderByDirection][];
  startAfter?: any[];
  limit?: number;
  query: QueryFn<T>;
}

type Listener<T> = MemoryQueryOptions<T> & {
  lastSnapshot?: QuerySnapshot<T>;
  subscriber: Subscriber<QuerySnapshot<T>>;
};

type Store<T> = Map<string, Snapshot<T>>;

export class MemoryQuery<T, C extends any[]> implements Query<T, C> {
  private readonly options: MemoryQueryOptions<T>;

  constructor(
    readonly store: Store<T>,
    protected readonly listeners: Map<string, Listener<T>>,
    options?: MemoryQueryOptions<T>,
  ) {
    this.options = {
      ...options,
      orderBy: options?.orderBy ?? [[ID, 'asc']],
      query:
        options?.query ??
        ((store: Map<string, Snapshot<T>>) => {
          return Array.from(store.values());
        }),
    };
  }

  static match<V>(val1: V, op: Operator<V>, val2: V) {
    if (op === '==') {
      return val1 === val2;
    }
    if (op === '<') {
      return val1 < val2;
    }
    if (op === '<=') {
      return val1 <= val2;
    }
    if (op === '>') {
      return val1 > val2;
    }
    if (op === '>=') {
      return val1 >= val2;
    }
    if (op === 'array-contains') {
      return (val2 as any).every((v: any) =>
        (val1 as any).some((v2: any) => v2 === v),
      );
    }
    if (op === 'array-contains-any') {
      return (val2 as any).some((v: any) =>
        (val1 as any).some((v2: any) => v2 === v),
      );
    }
    if (op === 'in') {
      return (val2 as any).some((v: any) => v === val2);
    }
  }
  where<K extends keyof T>(
    field: K,
    op: Operator<T[K]>,
    value: T[K],
  ): Query<T, C> {
    return new MemoryQuery<T, C>(this.store, this.listeners, {
      ...this.options,
      query: (store: Map<string, Snapshot<T>>) => {
        return this.options
          .query(store)
          .filter(item => MemoryQuery.match(item.data[field], op, value));
      },
    });
  }
  limit(limit: number): Query<T, C> {
    return new MemoryQuery(this.store, this.listeners, {
      ...this.options,
      limit,
    });
  }
  orderBy<K extends keyof T | typeof ID>(
    field: K,
    direction?: OrderByDirection,
  ): QueryBy<T, K> {
    const { startAfter, ...rest } = this.options;
    return new MemoryQuery(this.store, this.listeners, {
      ...rest,
      orderBy: [[field, direction ?? 'asc']],
    });
  }
  thenBy<K extends keyof T | typeof ID>(
    field: K,
    direction?: OrderByDirection,
  ): QueryBy<T, K, C> {
    const { startAfter, ...rest } = this.options;
    return new MemoryQuery(this.store, this.listeners, {
      ...rest,
      orderBy: [...this.options.orderBy, [field, direction ?? 'asc']],
    });
  }
  startAfter(...cursor: C): Query<T, C> {
    return new MemoryQuery(this.store, this.listeners, {
      ...this.options,
      startAfter: cursor,
    });
  }
  async get(): Promise<QuerySnapshot<T>> {
    const compare = (a: any, b: any) => (a < b ? -1 : b < a ? 1 : 0);
    const ordAny: Ord<any> = {
      compare,
      equals: (a, b) => compare(a, b) === 0,
    };
    const orderBy: Ord<Snapshot<T>> = fold(getMonoid<Snapshot<T>>())(
      this.options.orderBy.map(([field, direction]) =>
        ord.contramap(
          direction === 'asc' ? ordAny : getDualOrd(ordAny),
          (snapshot: Snapshot<T>) =>
            field === ID ? snapshot.id : snapshot.data[field],
        ),
      ),
    );
    let sorted = sort(orderBy)(this.options.query(this.store));
    if (this.options.startAfter) {
      const marker = zip(this.options.orderBy, this.options.startAfter).reduce<
        Snapshot<T>
      >(
        (obj, next) => {
          return (next[0][0] === ID
            ? { id: next[1], data: obj.data }
            : {
                id: obj.id,
                data: { ...obj.data, [next[0][0]]: next[1] },
              }) as Snapshot<T>;
        },
        { id: '', data: {} } as Snapshot<T>,
      );
      const index = sorted.findIndex(snapshot => {
        return orderBy.compare(snapshot, marker) === 1;
      });
      sorted = index >= 0 ? sorted.slice(index) : [];
    }
    return {
      items: sorted,
      changes: () =>
        sorted.map((item, newIndex) => ({
          item,
          type: 'added',
          oldIndex: -1,
          newIndex,
        })),
    };
  }
  onSnapshot(): Observable<QuerySnapshot<T>> {
    return new Observable(subscriber => {
      const id = v4();
      const listener = {
        ...this.options,
        subscriber,
      };
      this.listeners.set(id, listener);
      this.callListener(listener);
      return () => {
        this.listeners.delete(id);
      };
    });
  }
  private async callListener(listener: Listener<T>) {
    const query = new MemoryQuery(this.store, this.listeners, listener);
    const snapshot = await query.get();
    if (listener.lastSnapshot) {
      const lastSnapshot = listener.lastSnapshot;
      snapshot.changes = () => {
        const { added, updated, removed } = diff(
          lastSnapshot.items,
          snapshot.items,
          'id',
          {
            compareFunction: (a, b) =>
              a.id === b.id &&
              a.createdAt === b.createdAt &&
              a.updatedAt === b.updatedAt,
          },
        );
        const findIndex = (items: Snapshot<T>[], id: string) =>
          items.findIndex(item => item.id === id);
        return [
          ...added.map(item => ({
            type: 'added' as const,
            item,
            oldIndex: -1,
            newIndex: findIndex(snapshot.items, item.id),
          })),
          ...updated.map(item => ({
            type: 'modified' as const,
            item,
            oldIndex: findIndex(lastSnapshot.items, item.id),
            newIndex: findIndex(snapshot.items, item.id),
          })),
          ...removed.map(item => ({
            type: 'removed' as const,
            item,
            oldIndex: findIndex(snapshot.items, item.id),
            newIndex: -1,
          })),
        ];
      };
    }
    listener.lastSnapshot = snapshot;
    listener.subscriber.next(snapshot);
  }

  async callListeners() {
    await Promise.all(
      Array.from(this.listeners.values()).map(listener =>
        this.callListener(listener),
      ),
    );
  }
}

type Rollback = () => void;

export class MemoryRepository<T> extends MemoryQuery<T, [string]>
  implements Repository<T> {
  constructor() {
    super(new Map(), new Map());
  }
  static evalSentinels<T>(data: WithSentinels<T>, old?: T): T {
    const evalSentinel = (val: any, oldVal?: any) => {
      if (
        typeof val === 'object' &&
        val.incBy &&
        typeof val.incBy === 'number' &&
        typeof oldVal === 'number'
      ) {
        return oldVal + val.incBy;
      }
      if (val === 'serverTimestamp') {
        return new Date();
      }
      if (val === DELETE) {
        return undefined;
      }
      if (typeof val === 'object' && Array.isArray(oldVal)) {
        const add = val.add;
        if (Array.isArray(add)) {
          return [...oldVal, ...add];
        }
        const remove = val.remove;
        if (Array.isArray(remove)) {
          return oldVal.filter(item =>
            remove.every((item2: any) => item2 !== item),
          );
        }
      }

      return val;
    };
    return Object.keys(data).reduce<any>((prev, next) => {
      const newVal = (data as any)[next];
      const oldVal = old ? (old as any)[next] : undefined;
      prev[next] = evalSentinel(newVal, oldVal);
      return prev;
    }, {} as T);
  }
  async getOne(id: string): Promise<Snapshot<T> | undefined> {
    const snapshot = this.store.get(id);
    if (snapshot) {
      return snapshot;
    }
    return undefined;
  }
  async insert(
    data: WithSentinels<T>,
    id?: string,
    rollbacks?: Rollback[],
  ): Promise<{ id: string; createdAt: bigint }> {
    if (id && this.store.has(id)) {
      throw new Error('Already exists');
    }
    const newId = id ?? v4();
    const createdAt = process.hrtime.bigint();
    this.store.set(newId, {
      id: newId,
      createdAt,
      updatedAt: createdAt,
      data: MemoryRepository.evalSentinels(data),
    });
    if (rollbacks) {
      rollbacks.push(() => this.store.delete(newId));
    } else {
      await this.callListeners();
    }
    return { id: newId, createdAt };
  }
  async upsert(
    id: string,
    data: WithSentinels<T>,
    rollbacks?: Rollback[],
  ): Promise<bigint> {
    const obj = this.store.get(id);
    if (!obj) {
      const { createdAt } = await this.insert(data, id);
      if (rollbacks) {
        rollbacks.push(() => this.store.delete(id));
      } else {
        await this.callListeners();
      }
      return createdAt;
    }
    const updatedAt = process.hrtime.bigint();
    this.store.set(id, {
      ...obj,
      updatedAt,
      data: MemoryRepository.evalSentinels(data, obj.data),
    });
    if (rollbacks) {
      rollbacks.push(() => this.store.set(id, obj));
    } else {
      await this.callListeners();
    }
    return updatedAt;
  }
  async update(
    id: string,
    data: Partial<WithSentinels<T>>,
    precondition?: { lastUpdated: bigint },
    rollbacks?: Rollback[],
  ): Promise<bigint> {
    const obj = this.store.get(id);
    if (!obj) {
      throw new Error('Not found');
    }
    if (precondition && obj.updatedAt !== precondition.lastUpdated) {
      throw new Error('Precondition failed');
    }
    const updatedAt = process.hrtime.bigint();
    this.store.set(id, {
      ...obj,
      updatedAt,
      data: MemoryRepository.evalSentinels(
        { ...obj.data, ...data } as any,
        obj.data,
      ),
    });
    if (rollbacks) {
      rollbacks.push(() => this.store.set(id, obj));
    } else {
      await this.callListeners();
    }
    return updatedAt;
  }
  async delete(
    id: string,
    precondition?: { lastUpdated: bigint },
    rollbacks?: Rollback[],
  ): Promise<bigint> {
    const updatedAt = process.hrtime.bigint();
    const obj = this.store.get(id);
    if (precondition) {
      if (!obj) {
        throw new Error('Not found');
      }
      if (precondition.lastUpdated !== obj.updatedAt) {
        throw new Error('Precondition failed');
      }
    }
    this.store.delete(id);
    if (rollbacks) {
      if (obj) {
        rollbacks.push(() => this.store.set(id, obj));
      }
    } else {
      await this.callListeners();
    }
    return updatedAt;
  }
}

class MemoryTranScope<T> implements TranScope<T> {
  private readonly actions: ((rollbacks: Rollback[]) => Promise<void>)[] = [];
  constructor(private readonly repo: MemoryRepository<any>) {}
  get(query: Query<T, any>): Promise<QuerySnapshot<T>> {
    return (query as MemoryQuery<any, any>).get();
  }
  getOne(id: string): Promise<Snapshot<T> | undefined> {
    return this.repo.getOne(id);
  }
  insert(data: WithSentinels<T>, id?: string): TranScope<T> {
    this.actions.push(async rollbacks => {
      await this.repo.insert(data, id, rollbacks);
    });
    return this;
  }
  upsert(id: string, data: WithSentinels<T>) {
    this.actions.push(async rollbacks => {
      await this.repo.upsert(id, data, rollbacks);
    });
    return this;
  }
  update(
    id: string,
    data: Partial<WithSentinels<T>>,
    precondition?: { lastUpdated: bigint },
  ) {
    this.actions.push(async rollbacks => {
      await this.repo.update(id, data, precondition, rollbacks);
    });
    return this;
  }
  delete(id: string, precondition?: { lastUpdated: bigint }) {
    this.actions.push(async rollbacks => {
      await this.repo.delete(id, precondition, rollbacks);
    });
    return this;
  }

  async commit(): Promise<Either<Rollback[], Rollback[]>> {
    const rollbacks: Rollback[] = [];
    try {
      for (const action of this.actions) {
        await action(rollbacks);
      }
      return right(rollbacks);
    } catch (error) {
      return left(rollbacks);
    }
  }

  async callListeners() {
    return this.repo.callListeners();
  }
}

class MemoryTransaction implements Transaction {
  private readonly scopes: MemoryTranScope<any>[] = [];
  with<T>(repo: Repository<T>): TranScope<T> {
    const scope = new MemoryTranScope<T>(repo as any);
    this.scopes.push(scope);
    return scope;
  }
  async commit() {
    const rollbacks = await Promise.all(
      this.scopes.map(scope => scope.commit()),
    );
    if (rollbacks.some(rb => isLeft(rb))) {
      rollbacks.forEach(rollback => {
        const rb = isLeft(rollback) ? rollback.left : rollback.right;
        rb.reverse().forEach(undoAction => undoAction());
      });
      return false;
    }
    await Promise.all(this.scopes.map(scope => scope.callListeners()));
    return true;
  }
}

export class MemoryTransactionManager implements TransactionManager {
  async runTransaction<U>(
    callback: (tran: Transaction) => Promise<U>,
    options?: { maxAttempts?: number },
  ): Promise<U> {
    let count = 0;
    while (count++ < (options?.maxAttempts ?? 1)) {
      const tran = new MemoryTransaction();
      const retval = await callback(tran);
      if (await tran.commit()) {
        return retval;
      }
    }
    throw new Error('Tran failed');
  }
}
