import {
  Repository,
  Query,
  Snapshot,
  WithSentinels,
  QueryBy,
  QuerySnapshot,
  ID,
  TranScope,
  Transaction as Tran,
  TransactionManager,
  Operator,
} from './repository';
import { Observable } from 'rxjs';
import {
  CollectionReference,
  Query as CollectionQuery,
  DocumentData,
  FirestoreDataConverter,
  FieldPath,
  Timestamp,
  OrderByDirection,
  QueryDocumentSnapshot,
  QuerySnapshot as QueryCollectionSnapshot,
  Transaction,
  DocumentReference,
  DocumentSnapshot,
  Firestore,
} from '@google-cloud/firestore';
import * as t from 'io-ts';
import { isLeft } from 'fp-ts/lib/Either';
import { failure } from 'io-ts/lib/PathReporter';
import { withSentinels, getProps } from './firestoreRepositoryUtils';

const NANO_SEC_PER_SEC = BigInt(1000000000);
const getNanoEpochFromTimeStamp = (timestamp: Timestamp) =>
  BigInt(timestamp.nanoseconds) + BigInt(timestamp.seconds) * NANO_SEC_PER_SEC;

const getTimeStampFromNanoEpoch = (nano: bigint) =>
  new Timestamp(
    Number(nano / NANO_SEC_PER_SEC),
    Number(nano % NANO_SEC_PER_SEC),
  );

interface FirestoreQueryOptions<T> {
  converter: FirestoreDataConverter<T>;
  orderBy: [keyof T | typeof ID, OrderByDirection][];
  startAfter?: any[];
  query?: CollectionQuery<DocumentData>;
}

class FirestoreQuery<S extends t.Any, C extends any[]>
  implements Query<t.TypeOf<S>, C> {
  protected readonly options: FirestoreQueryOptions<t.TypeOf<S>>;
  constructor(
    private readonly query:
      | (() => CollectionQuery<DocumentData>)
      | CollectionQuery<DocumentData>,
    protected readonly schema: S,
    options: Partial<FirestoreQueryOptions<t.TypeOf<S>>>,
  ) {
    this.options = {
      ...options,
      converter: options.converter ?? this.createConverter(),
      orderBy: options.orderBy ?? [[ID, 'asc']],
    };
  }
  private createConverter() {
    const codec = withSentinels(this.schema);
    return {
      fromFirestore: (data: DocumentData): t.TypeOf<S> => {
        const obj = codec.decode(data);
        if (isLeft(obj)) {
          throw new Error(failure(obj.left).join('\n'));
        }
        return obj.right;
      },
      toFirestore: (obj: t.TypeOf<S>) => {
        return codec.encode(obj);
      },
    };
  }
  private getQuery(): CollectionQuery<DocumentData> {
    if (typeof this.query === 'function') {
      return this.query();
    }
    return this.query;
  }
  where<K extends keyof t.TypeOf<S>>(
    field: K,
    op: Operator<t.TypeOf<S>[K]>,
    value: t.TypeOf<S>[K],
  ): Query<t.TypeOf<S>, C> {
    return new FirestoreQuery(
      this.getQuery().where(
        field as any,
        op,
        getProps(this.schema)![field as any].encode(value),
      ),
      this.schema,
      this.options,
    );
  }
  limit(limit: number): Query<t.TypeOf<S>, C> {
    return new FirestoreQuery(
      this.getQuery().limit(limit),
      this.schema,
      this.options,
    );
  }
  orderBy<K extends keyof t.TypeOf<S> | typeof ID>(
    field: K,
    direction?: OrderByDirection,
  ): QueryBy<t.TypeOf<S>, K> {
    const { startAfter, ...rest } = this.options;
    return new FirestoreQuery(this.query, this.schema, {
      ...rest,
      orderBy: [[field, direction ?? 'asc']],
    });
  }
  thenBy<K extends keyof t.TypeOf<S> | typeof ID>(
    field: K,
    direction?: OrderByDirection,
  ): QueryBy<t.TypeOf<S>, K, C> {
    const { startAfter, ...rest } = this.options;
    return new FirestoreQuery(this.query, this.schema, {
      ...rest,
      orderBy: [...this.options.orderBy, [field, direction ?? 'asc']],
    });
  }
  startAfter(...cursor: C): Query<t.TypeOf<S>, C> {
    return new FirestoreQuery(this.query, this.schema, {
      ...this.options,
      startAfter: cursor,
    });
  }
  prepareQuery() {
    let query = this.options.orderBy.reduce<CollectionQuery<DocumentData>>(
      (prev, next) => {
        if (next[0] === ID) {
          return prev.orderBy(FieldPath.documentId(), next[1]);
        }
        return prev.orderBy(next[0] as any, next[1]);
      },
      this.getQuery(),
    );
    if (this.options.startAfter) {
      query = query.startAfter(...this.options.startAfter);
    }
    return query.withConverter(this.options.converter);
  }
  static toQuerySnapshot<T>(
    snapshot: QueryCollectionSnapshot<T>,
  ): QuerySnapshot<T> {
    const mapDoc = (doc: QueryDocumentSnapshot<T>) => ({
      id: doc.id,
      data: doc.data(),
      createdAt: getNanoEpochFromTimeStamp(doc.createTime),
      updatedAt: getNanoEpochFromTimeStamp(doc.updateTime),
    });
    return {
      changes: () =>
        snapshot.docChanges().map(change => ({
          item: mapDoc(change.doc as any),
          type: change.type,
          oldIndex: change.oldIndex,
          newIndex: change.newIndex,
        })),
      items: snapshot.docs.map(mapDoc),
    };
  }

  async get(): Promise<QuerySnapshot<t.TypeOf<S>>> {
    return FirestoreQuery.toQuerySnapshot(await this.prepareQuery().get());
  }
  onSnapshot(): Observable<QuerySnapshot<t.TypeOf<S>>> {
    const query = this.prepareQuery();
    return new Observable(subscriber =>
      query.onSnapshot(
        snapshot => {
          subscriber.next(FirestoreQuery.toQuerySnapshot(snapshot));
        },
        error => {
          subscriber.error(error);
        },
      ),
    );
  }
}

export class FirestoreRepository<S extends t.Any>
  extends FirestoreQuery<S, [string]>
  implements Repository<t.TypeOf<S>> {
  private readonly updateCodec: t.Any;
  constructor(
    private readonly collection: () => CollectionReference<DocumentData>,
    protected readonly schema: S,
  ) {
    super(collection, schema, {});
    this.updateCodec = t.partial(getProps(withSentinels(this.schema)) as any);
  }

  getRef(id?: string): DocumentReference<t.TypeOf<S>> {
    const ref = this.collection().withConverter(this.options.converter);
    if (id) {
      return ref.doc(id);
    }
    return ref.doc();
  }

  async getOne(id: string): Promise<Snapshot<t.TypeOf<S>> | undefined> {
    return FirestoreRepository.toSnapshot(await this.getRef(id).get());
  }

  static toSnapshot<T>(snapshot: DocumentSnapshot<T | undefined>) {
    const doc = snapshot.data();
    if (snapshot.createTime && snapshot.updateTime && doc) {
      return {
        id: snapshot.id,
        data: doc,
        createdAt: getNanoEpochFromTimeStamp(snapshot.createTime),
        updatedAt: getNanoEpochFromTimeStamp(snapshot.updateTime),
      };
    }
    return undefined;
  }

  async insert(
    data: WithSentinels<t.TypeOf<S>>,
    id?: string,
  ): Promise<{ id: string; createdAt: bigint }> {
    const ref = this.getRef(id);
    return {
      id: ref.id,
      createdAt: getNanoEpochFromTimeStamp((await ref.create(data)).writeTime),
    };
  }
  async upsert(id: string, data: WithSentinels<t.TypeOf<S>>): Promise<bigint> {
    const db = this.collection().withConverter(this.options.converter);
    const docRef = db.doc(id);
    return getNanoEpochFromTimeStamp((await docRef.set(data)).writeTime);
  }

  static toPrecondition(lastUpdated?: bigint) {
    if (lastUpdated !== undefined) {
      return {
        lastUpdateTime: getTimeStampFromNanoEpoch(lastUpdated),
      };
    }
  }
  prepareUpdate(
    data: Partial<WithSentinels<t.TypeOf<S>>>,
    precondition?: { lastUpdated: bigint },
  ) {
    const args = Object.entries(this.updateCodec.encode(data as any)).reduce<
      any[]
    >((prev, next) => [...prev, ...next], []);
    if (args.length === 0) {
      throw new Error('empty update');
    }
    if (precondition) {
      args.push(FirestoreRepository.toPrecondition(precondition.lastUpdated));
    }
    return args;
  }
  async update(
    id: string,
    data: Partial<WithSentinels<t.TypeOf<S>>>,
    precondition?: { lastUpdated: bigint },
  ): Promise<bigint> {
    const args = this.prepareUpdate(data, precondition);
    return getNanoEpochFromTimeStamp(
      (await (this.getRef(id).update as any)(...args)).writeTime,
    );
  }
  async delete(
    id: string,
    precondition?: { lastUpdated: bigint },
  ): Promise<bigint> {
    return getNanoEpochFromTimeStamp(
      (
        await this.getRef(id).delete(
          FirestoreRepository.toPrecondition(precondition?.lastUpdated),
        )
      ).writeTime,
    );
  }
}

class FirestoreTranScope<T> implements TranScope<T> {
  constructor(
    private readonly tran: Transaction,
    private readonly repo: FirestoreRepository<any>,
  ) {}
  async get(query: Query<T, any>): Promise<QuerySnapshot<T>> {
    return FirestoreQuery.toQuerySnapshot(
      await this.tran.get((query as FirestoreQuery<any, any>).prepareQuery()),
    );
  }
  async getOne(id: string): Promise<Snapshot<T> | undefined> {
    return FirestoreRepository.toSnapshot(
      await this.tran.get(this.repo.getRef(id)),
    );
  }
  insert(data: WithSentinels<T>, id?: string): TranScope<T> {
    const ref = this.repo.getRef(id);
    this.tran.create(ref, data);
    return this;
  }
  upsert(id: string, data: WithSentinels<T>) {
    const ref = this.repo.getRef(id);
    this.tran.set(ref, data);
    return this;
  }
  update(
    id: string,
    data: Partial<WithSentinels<T>>,
    precondition?: { lastUpdated: bigint },
  ) {
    const args = this.repo.prepareUpdate(data, precondition);
    (this.tran.update as any)(this.repo.getRef(id), ...args);
    return this;
  }
  delete(id: string, precondition?: { lastUpdated: bigint }) {
    this.tran.delete(
      this.repo.getRef(id),
      FirestoreRepository.toPrecondition(precondition?.lastUpdated),
    );
    return this;
  }
}

class FirestoreTransaction implements Tran {
  constructor(private readonly tran: Transaction) {}
  with<T>(repo: Repository<T>): TranScope<T> {
    return new FirestoreTranScope(this.tran, repo as any);
  }
}

export class FirestoreTransactionManager implements TransactionManager {
  constructor(private readonly firestore: Firestore) {}
  runTransaction<U>(
    callback: (tran: Tran) => Promise<U>,
    options?: { maxAttempts?: number },
  ): Promise<U> {
    return this.firestore.runTransaction((tran: Transaction) => {
      return callback(new FirestoreTransaction(tran));
    }, options);
  }
}
