import * as t from 'io-ts';
import { DELETE } from '.';
import { FieldValue } from '@google-cloud/firestore';

const withEncode = <A>(codec: t.Type<A, any>, encode: t.Encode<A, any>) =>
  new t.Type<A, any>(codec.name, codec.is, codec.validate, encode);

const numberSentinel = withEncode(t.type({ incBy: t.Int }), a =>
  FieldValue.increment(a.incBy),
);

const dateSentinel = withEncode(t.literal('serverTimestamp'), () =>
  FieldValue.serverTimestamp(),
);

const arraySentinel = (codec: t.Any) =>
  t.union([
    withEncode(t.type({ add: codec }), a => FieldValue.arrayUnion(...a.add)),
    withEncode(t.type({ remove: codec }), a =>
      FieldValue.arrayRemove(...a.remove),
    ),
  ]);

const symbol = <S extends Symbol>(sym: S) =>
  new t.Type<S>(
    'Symbol',
    (u): u is S => u === sym,
    (u, c) => (u === sym ? t.success(sym) : t.failure(u, c)),
    u => u,
  );

const deleteSentinel = withEncode(symbol(DELETE), () => FieldValue.delete());

function sentinel(codec: t.Any): t.Any | undefined {
  return codec instanceof t.NumberType
    ? t.union([numberSentinel, deleteSentinel])
    : codec instanceof t.ArrayType
    ? t.union([arraySentinel(codec), deleteSentinel])
    : codec.is(new Date())
    ? t.union([dateSentinel, deleteSentinel])
    : deleteSentinel;
}

export function getProps(codec: t.Any): t.Props | undefined {
  const c = codec as any;
  if (c.props) {
    return c.props;
  }
  if (c.type) {
    return getProps(c.type);
  }
  if (c.types) {
    return (c.types as t.Any[]).reduce<t.Props>((prev, type) => {
      const props = getProps(type);
      if (props) {
        Object.assign(prev, props);
      }
      return prev;
    }, {});
  }
}

export function withSentinels(codec: t.Any): t.Any {
  const propsWithSentinels = (props: t.Props): t.Props =>
    Object.keys(props).reduce<t.Props>((prev, key) => {
      prev[key] = withSentinels(props[key]);
      return prev;
    }, {});
  const arrayWithSentinels = (codecs: t.Any[]): t.Any[] =>
    codecs.map(codec => withSentinels(codec));
  if (codec instanceof t.InterfaceType) {
    return t.interface(propsWithSentinels(codec.props), codec.name);
  }
  if (codec instanceof t.PartialType) {
    return t.partial(propsWithSentinels(codec.props), codec.name);
  }
  if (codec instanceof t.IntersectionType) {
    return t.intersection(arrayWithSentinels(codec.types) as any, codec.name);
  }
  if (codec instanceof t.UnionType) {
    return t.union(arrayWithSentinels(codec.types) as any, codec.name);
  }
  if (codec instanceof t.ExactType) {
    return t.exact(withSentinels(codec) as any, codec.name);
  }
  if (codec instanceof t.RefinementType) {
    const s = sentinel(codec.type);
    if (s) {
      return t.union([s, codec]);
    }
    return codec;
  }
  if (codec instanceof t.ReadonlyType) {
    return t.readonly(withSentinels(codec.type) as any, codec.name);
  }
  if (codec instanceof t.ReadonlyArrayType) {
    return t.readonlyArray(withSentinels(codec.type), codec.name);
  }
  const s = sentinel(codec);
  if (s) {
    return t.union([s, codec]);
  }
  return codec;
}
