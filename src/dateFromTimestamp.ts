import * as t from 'io-ts';
import { Timestamp } from '@google-cloud/firestore';

export const DateFromTimestamp = new t.Type<Date, Timestamp, unknown>(
  'DateFromTimestamp',
  (u): u is Date => u instanceof Date,
  (u, c) => {
    if (u instanceof Timestamp) {
      const d = u.toDate();
      return isNaN(d.getTime()) ? t.failure(u, c) : t.success(d);
    }
    return t.failure(u, c);
  },
  d => Timestamp.fromDate(d),
);
