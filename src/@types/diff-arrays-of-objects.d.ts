declare module 'diff-arrays-of-objects' {
  function diff<T>(
    first: T[],
    second: T[],
    idField: keyof T,
    options?: {
      compareFunction?: (a: T, b: T) => boolean;
    },
  ): { same: T[]; added: T[]; updated: T[]; removed: T[] };

  export = diff;
}
